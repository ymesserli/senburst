'use strict';

var mapKey = function(array, fct){
    var values = {};
    $.each(array, function(i, val) {
        var r = fct(i, val);
        values[r[0]] = r[1];
    });
    return values;
}

angular.module('patentsVisualApp')
  .service('Patents', function Patents($q, $rootScope) {
	

  	// Interface:
	var o = {};

	// Db:
    var db = null;

    // All the fields, template
    var fields_tpl = {};

    // var global tot:
    var global_tot = 0;

	// Is the data loaded?
	var defer = $q.defer();

	o.stats = function(__filters){

        var filters = __filters || {};

    
        var tot = db(filters).count();


        var scale = d3.scale.linear().range([0, 100]).domain([0, tot]);

        // var t = 0; 
        // 
        var computeStateFor = function(name){
            // If there is a query, use the quey:
            var array_to_process = filters[ name ] || db().distinct(name);
            console.log(array_to_process)
            return mapKey(  array_to_process, function(i, el){
                var query  = $.extend({}, filters);
                query[ name ] = el;
                var r = db(query).count();
                if( isNaN(r) ) console.log(name, el, r);
                return [ el,  scale( r ) ];
            });
        }
        
        // console.log(t)
        //console.log(computeStateFor("cited_type"))
        console.log(fields_tpl);
		return {

            cited_type:  $.extend( {}, fields_tpl.cited_type,  computeStateFor("cited_type")),
            year:       $.extend( {}, fields_tpl.year,       computeStateFor("year")),
            company:    $.extend( {}, fields_tpl.company,    computeStateFor("company"))

        };
	}

    o.total = function(filters) {
        var scale = d3.scale.linear().range([0, 100]).domain([0, global_tot]);
         return scale( db(filters).count() );
    }

	o.init = function(fct){

		// Put all the data in the DB:
        
        $.getJSON('/data/patents.json', function(data){
            
            db = TAFFY();
            db.settings({
                onInsert: function(){
                    for (var key in this) {
                        if( !isNaN(this[key])  )
                            this[key] = this[key].toString();
                        //else console.log( this[key] )
                    };
                }
            });
            db.insert(data.patents);

            var base_field = function(name){
                return mapKey( db().distinct(name), function(i, el){
                    return [ el, 0 ];
                });
            }

            fields_tpl = {
                cited_type: base_field("cited_type"),
                year: base_field("year"),
                company: base_field("company")
            }
            
            
            global_tot = db().count();

            // console.log("cited",  db({ cited_type: ["2", "3"] }).count() );

            // console.log( "year", db({ year: ["2001", "1996", "2005"] }).count() );

           // console.log( db().distinct("classification") );
        }).fail(function(e2, a, e) {
            console.log( e );
        }); 
		
        setTimeout(function(){ defer.resolve(); $rootScope.$apply(); }, 2000);

       

        return defer.promise;
		
	}


  	return o;
});
