'use strict';

angular.module('patentsVisualApp')
  .directive('ngAdaptiveSize', function () {

    return function(scope, element, attrs) {

    	var setSize = function(){
	      	//var parent = element.parent(),
	      	var w = $(window).width(), 
	      	h = $(window).height();

	      	// if no height, take the windows size
	      	// if(h == 0) h = $(document).height();

	      	// define the square:
	      	var s = Math.min(w, h);
	        
	        // Setup a square size and margin:
	        element.css({
	        	height: s,
	        	width: s,
	        	'margin': ((h - s) / 2) + "px " + ((w - s) / 2) + "px"
	        });
	    }

        // Setup binding:
        $(window).resize(setSize); $(document).resize(setSize);

        // Initialize:
        setSize();
    }
});
