'use strict';

angular.module('patentsVisualApp')
  .directive('ngSenburst', function () {



    return {
      	template: '<svg></svg>',
      	replace: true,
      	scope: {
      		data: "=",
      		global: "=",
      		onClick: "=",
      		reset: "="
      	},
      	restrict: 'E',
      	link: function postLink(scope, element, attrs) {
      	  	


      		// SETTINGS:
		  	// How to display arc:
		  	var innerRadius = 60, outerRadius = 100;
		  	// all the pies:
		  	var pies = {};

		  	// all the colors:
		    var colors =  ([
		    	[ "#000", "#fff" ],
		    	[ "#FFD13A", "#BDFFAF", "#DC2AEE", "#90A3FF", "#10D5FF"],
		    	[ "#FF000B", "#0032FF", "#FFE79A", "#56AD9A", "#bcbddc", "#756bb1" ],
		    	[ "#FF000B", "#0032FF", "#FFE79A", "#56AD9A", "#bcbddc", "#756bb1" ],
		    ]);


		    // { start: "#000", end: "#fff"},
		    // 	{ start: "#FF000B", end: "#0032FF"},
		    // 	{ start: "#FF000B", end: "#0032FF"},
		    // 	{ start: "#bcbddc", end: "#756bb1"},
		    // 	{ start: "#FEF88A", end: "#3D641D"},
		    // 	{ start: "#FFE79A", end: "#56AD9A"},
		    // 	{ start: "", end: ""}


		    // The overall value:
		    var global_init = scope.global;
		    

		    // Global element:
		    var svg = d3.select(element[0]);

		  	// Function to create new pie:
		  	// Note: innerRadius and outRadius are shadowed
		  	var createPie = function(name, __data, innerRadius, outerRadius, c){


		  		// interface:
		  		var pie = {
		  			name: name,
		  			labels: [],
		  			values: [],
		  			layout: null
		  		};




		  		// function to setup new value:
		  		var setValue = function(data){

		  			// Reset:
		  			pie.labels = [];
		  			pie.values = [];

		  			// Get the data:
				    for (var key in data) {
					    pie.labels.push(key);
					    pie.values.push(data[key]);
					}

		  		}
		  		

		  		// Create the layout:
		  		pie.layout = d3.layout.pie().value(function(d, i) { 
		  			//if(pie.name == "cited_type") console.log(i); 
		  			return pie.values[i]; 
		  		}).sort(null);

		  		// create the arc:
		  		var arc = d3.svg.arc()
			    .innerRadius(innerRadius)
			    .outerRadius(outerRadius);

			    // Store the displayed angles in _current.
				// Then, interpolate from _current to the new angles.
				// During the transition, _current is updated in-place by d3.interpolate.
				function arcTween(a) {
				  var i = d3.interpolate(this._current, a);
				  
				  this._current = i(0);
				  return function(t) {
				    return arc(i(t));
				  };
				}
			    
				// Init the values
				setValue(__data);


				// If there is a function passed, exec 
				// on click of the partition
				// and path the name of the partiion
				// with the label
				var exec_cmd = function(a, i){
					scope.onClick(this, pie, pie.labels[i], pie.values[i]  );
				}

			    pie.dom = svg.append('g')
			    .attr('id', pie.name)
			    .datum( d3.range(0, pie.values.length ) );

			    var colors = pie.name == "year" ? d3.scale.linear()
			    .domain([1, pie.values.length/4, pie.values.length/3, pie.values.length/4, pie.values.length ])
			    .range(c) : d3.scale.category20();

			    var path = pie.dom.selectAll("path")
			    .data(pie.layout)
			    .enter().append("path")
			      .attr("fill", function(d, i) { 
			      	 if ( pie.name == "year" )
			      	 	return colors( i  ); 
			      	 else
			      	 	return colors( Math.random() * 20 );
			      })
			      //.attr("fill", function(d, i) { return    "hsl(" + Math.random() * 360 + ",100%,50%)"; })
			      .attr("d", arc)
			      .on("click", exec_cmd)
			      .each(function(d) {   this._current = d; })  // store the initial angles
			      // Then recenter the path to appear in the middle:



			    pie.change = function(new_data){
			    	
			    	setValue(new_data);
				    path = path.data(pie.layout); // compute the new angles
				    path.transition().duration(500).attrTween("d", arcTween); // redraw the arcs
			    }


		  		return pie;

		  	}

      		// Setup the root svg element:
      		svg.attr("viewBox",'0,0,210,210');
      		// Center it in the middle of the page:
      		svg = svg.append("svg:g")
		    .attr("transform", "translate( 105, 105)");


		    // Compute the size of each pie:
		    var nbOfPie = Object.keys(scope.data).length, // !!WARNING keys is not supported in every browser
		    part = (outerRadius - innerRadius) / nbOfPie,
		    nb = 0;

		    

      		$.each(scope.data, function(key, el){

      			var start = innerRadius + nb * part;

      			
      			pies[ "" + key ] = createPie(key, el, start, start + part, colors[nb]);

      			nb++;

   
      			
      		});

      		


			// Redraw the pie reparticition each time the data change:
      	  	scope.$watch( 'data', function (v, t, this_scope) {
      	  		$.each(pies, function(key, pie){
      	  			
	   				pie.change(  this_scope.data["" + pie.name]  );
	      			
	      		});
      	          
      	    }, true);

      	  	/////////////////////////////////////////////////////////////////////
      	  	// Show a circle in the middle to represent the overall number:

      	  	
      	  	// function that return the percent
      	  	var global_radius = function(new_radius){
      	  		return new_radius / global_init * innerRadius;
      	  	}

      	  	// create the element:
      	  	var global_dom = svg.append("svg:circle")
      	  	.attr('cy', 0)
      	  	.attr('cx', 0)
      	  	.attr('r', innerRadius)
      	  	.on("click", scope.reset)
      	  	.style('fill', '#CFCFCF');


      	    // Redraw the pie reparticition each time the data change:
      	  	scope.$watch( 'global', function (v, t, this_scope) {
      	  		
      	  		global_dom.transition().attr("r", global_radius(scope.global)    );
      	          
      	    }, true);



      	}
    };
  });
