'use strict';

angular.module('patentsVisualApp')
  .controller('MainCtrl', function ($scope, Patents) {

    var a = Patents.stats();
    console.log(a)
    $scope.repartition =a;


    $scope.total = 100;

    var clickedEl = [];
    var filters = {}

    $scope.selectElement = function(el, pie, label, value){


        // Change color:
        var d = d3.select(el).attr('stroke', 'red')
        .attr('stroke-width', '0.5');

        // Register element
        // 
        if( filters[ pie.name ] == null ) filters[ pie.name ] = [label];
        else filters[ pie.name ].push( label );

        clickedEl.push(d);
        //console.log(el, pie, label, value)
        

    }

    $scope.reset = function(){
         $scope.repartition = Patents.stats();
         $scope.total = Patents.total();

         // Reset:
        filters = {};

        

        // Need to propage the change trough the scope architecture:
        // $scope.$digest();

        $.each(clickedEl, function(i, el){
            el.attr('stroke', null);
        });

        clickedEl = [];
    }


    $scope.refresh = function(){

        var t = Patents.stats(filters);
        console.log(t);
        $scope.repartition = t;
        $scope.total = Patents.total(filters);

        // Reset:
        filters = {};

        

        // Need to propage the change trough the scope architecture:
        // $scope.$digest();

        $.each(clickedEl, function(i, el){
            el.attr('stroke', null);
        });

        clickedEl = [];
    }



   
});
