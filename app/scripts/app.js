'use strict';

angular.module('patentsVisualApp', [])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        resolve: {
          loadData: function($q, $timeout, Patents) {
                // var deferred = $q.defer();
                // $timeout(function(){
                //     return deferred.resolve();
                // }, 2000);
                // return deferred.promise;
                return Patents.init();
            }
          }
      })
      .otherwise({
        redirectTo: '/'
      });
  });


