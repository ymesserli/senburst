'use strict';

describe('Directive: ngSenburst', function () {

  // load the directive's module
  beforeEach(module('patentsVisualApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<ng-senburst></ng-senburst>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the ngSenburst directive');
  }));
});
