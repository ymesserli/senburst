'use strict';

describe('Service: patents', function () {

  // load the service's module
  beforeEach(module('patentsVisualApp'));

  // instantiate service
  var patents;
  beforeEach(inject(function (_patents_) {
    patents = _patents_;
  }));

  it('should do something', function () {
    expect(!!patents).toBe(true);
  });

});
